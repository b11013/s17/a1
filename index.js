/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	 function printUserInfo(){
		let name = prompt("What is your name?");
		let age = prompt("How old are you?");
		let address = prompt("Where do you live?");
		
		console.log("Hello, " + name);
		console.log("You are " + age + "years old.");
		console.log("You live in " + address);

		alert("Thank you for your input!");
	}
	printUserInfo();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.	
*/

	//second function here:
	function top5Bands() {
		let bandNames = ["Franz Ferdinand", "Paramore", "My Chemical Romance", "System of a Down", "Green Day"]
		console.log("Top 5 Bands:");
		console.log(bandNames);
	}
	top5Bands();


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
		function top5Movies() {
		let movie1 = "Pulp Fiction";
		let movie2 = "Fight Club";
		let movie3 = "Interstellar";
		let movie4 = "Being John Malkovich";
		let movie5 = "Whiplash";
		let ratingMovie1 = "Rotten Tomatoes Rating: " + 92 + "%"
		let ratingMovie2 = "Rotten Tomatoes Rating: " + 79 + "%"
		let ratingMovie3 = "Rotten Tomatoes Rating: " + 72 + "%"
		let ratingMovie4 = "Rotten Tomatoes Rating: " + 94 + "%"
		let ratingMovie5 = "Rotten Tomatoes Rating: " + 94 + "%"


		console.log("1." + movie1);
		console.log(ratingMovie1);
		console.log("2." + movie2);
		console.log(ratingMovie2);
		console.log("3." + movie3);
		console.log(ratingMovie3);
		console.log("4." + movie4);
		console.log(ratingMovie4);
		console.log("5." + movie5);
		console.log(ratingMovie5);

	}
	top5Movies();



/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


function printFriends(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();